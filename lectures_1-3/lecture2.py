from decouple import config
from flask import Flask, request
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate



app = Flask(__name__)

db_user = config('DB_USER')
db_password = config('DB_PASSWORD')
db_address = config('DB_ADDRESS')
db_title = config('DB_TITLE')
app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql://{db_user}:{db_password}@{db_address}/{db_title}'

db = SQLAlchemy(app)
api = Api(app)
migrate = Migrate(app, db)

class Book(db.Model):
    __tablename__ = 'books'
    _id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=True)
    author = db.Column(db.String, nullable=False)
    description = db.Column(db.String, nullable=True)
    reader_id = db.Column(db.Integer, db.ForeignKey('readers._id'))
    reader = db.relationship('Reader')

    def __repr__(self):
        return f'<{self._id}> {self.title} with author {self.author}'

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class Reader(db.Model):
    __tablename__ = 'readers'
    _id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String, nullable=False)
    books = db.relationship('Book', backref='book', lazy=True)

    def __repr__(self):
        return f' New reader <{self._id}> {self.first_name} {self.last_name}'

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Books(Resource):
    def get(self):
        books = Book.query.all()
        return {"books": [b.as_dict() for b in books]}

    def post(self):
        data = request.get_json()
        new_book = Book(**data)
        print(new_book)
        db.session.add(new_book)
        db.session.commit()
        return new_book.as_dict()

class Readers(Resource):
    def get(self):
        readers = Reader.query.all()
        return {"readers": [r.as_dict() for r in readers]}

    def post(self):
        data = request.get_json()
        new_reader = Reader(**data)
        db.session.add(new_reader)
        db.session.commit()
        return new_reader.as_dict()

class Readers_Books(Resource):
    def get(self, reader_id):
        reader = Reader.query.get_or_404(reader_id)
        return {"data": [book.as_dict() for book in reader.books]}

db.create_all()
api.add_resource(Books, '/books')
api.add_resource(Readers, '/readers')
api.add_resource(Readers_Books, '/<int:reader_id>/books')

if __name__ == '__main__':
    app.run(debug=True)
