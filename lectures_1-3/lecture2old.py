from flask import Flask, request
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://root:root@172.19.0.3:5432/test_db'

db = SQLAlchemy(app)
api = Api(app)
migrate = Migrate(app, db)

class Book(db.Model):
    __tablename__ = 'books'
    _id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, nullable=False)
    author = db.Column(db.String, nullable=False)
    description = db.Column(db.String, nullable=True)

    def __repr__(self):
        return f'<{self.id}> {self.title} with author {self.author}'

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class Books(Resource):
    def post(self):
        data = request.get_json()
        new_book = Book(**data)
        db.session.add(new_book)
        db.session.commit()
        return new_book.as_dict()

db.create_all()
api.add_resource(Books, '/')

if __name__ == '__main__':
    app.run(debug=True)