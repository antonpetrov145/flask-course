from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS

app = Flask(__name__)
api = Api(app)

CORS(app)

class Book:
    _id =1
    def __init__(self, title, author):
        self.id = Book._id
        self.author = author
        self.title = title
        Book._id += 1

    def serialize(self):
        return self.__dict__

class Books(Resource):
    def get(self):
        return {'books': [b.serialize() for b in books]}

    def post(self):
        try:
            data = request.get_json(force=True)
            new_book = Book(data['title'], data['author'])
            books.append(new_book)
            return {'books': [b.serialize() for b in books]}
        except Exception:
            return {'error': 'Bad Request'}, 400

books = []
for i in range(1,11):
    books.append(Book(f'Title {i}', f'Author {i}'))

class Actions(Resource):
    def get(self, id):
        try:
            book = [b for b in books if b.id == id][0]
            return book.serialize()
        except IndexError:
            return {'error': 'Not Found'}, 404

    def put(self, id):
        try:
            updated_book = request.get_json(force=True)
            book = [b for b in books if b.id == id][0]
            book.title = updated_book['title']
            book.author = updated_book['author']
            return book.serialize()
        except IndexError:
            return {'error': 'Not Found for update'}, 404

    def delete(self, id):
        try:
            book = [b for b in books if b.id == id][0]
            books.remove(book)
            return [b.serialize() for b in books]
        except IndexError:
            return {'error': 'Not found for delete!'}, 404

api.add_resource(Books, '/')
api.add_resource(Actions, '/<int:id>')

if __name__ == '__main__':
    app.run(debug=True)
