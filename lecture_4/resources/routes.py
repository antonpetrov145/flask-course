from resources.auth import LoginComplainer, RegisterComplainer

routes = ((RegisterComplainer, "/register"), (LoginComplainer, "/login"))
