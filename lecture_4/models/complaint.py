from db import db
from sqlalchemy import func

from models.enums import State


class ComplaintModel(db.Model):
    __tablename__ = "complaints"

    pk = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text, nullable=False)
    photo_url = db.Column(db.String(100), nullable=False)
    amount = db.Column(db.Float, nullable=False)
    created_on = db.Column(db.DateTime, server_default=func.now())
    status = db.Column(db.Enum(State), default=State.pending, nullable=False)
    complainer_pk = db.Column(db.Integer, db.ForeignKey("complainers.pk"))
    complainer = db.relationship("ComplainerModel")
