from datetime import datetime, timedelta

import jwt
from decouple import config
from flask_httpauth import HTTPTokenAuth

# import because of the eval function
from models.user import ApproverModel, ComplainerModel
from werkzeug.exceptions import Unauthorized


class AuthManager:
    @staticmethod
    def encode_token(user):
        payload = {
            "sub": user.pk,
            "exp": datetime.utcnow() + timedelta(days=2),
            "type": user.__class__.__name__,
        }
        return jwt.encode(payload, key=config("SECRET_KEY"), algorithm="HS256")

    @staticmethod
    def decode_token(token):
        try:
            info = jwt.encode(jwt=token, key=config("SECRET_KEY"), algorithms=["HS256"])
            return info["sub"], info["type"]
        except Exception as e:
            return e


auth = HTTPTokenAuth(scheme="Bearer")


@auth.verify_token
def verify_token(token):
    try:
        user_pk, type_user = AuthManager.decode_token(token)
        return eval(f"{type_user}.query.filter_by(pk={user_pk}).first()")
    except Exception as e:
        return Unauthorized("Invalid or missing token!")
